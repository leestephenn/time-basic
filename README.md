# time-basic

This is a program I wrote to generate PDF schedules from the events in my day. It reads from CSV files and outputs LaTeX.

## Building

I build this program with cabal. To do so, run

```shell
cabal new-build
```

I then install it with

```shell
cabal new-install TimeBasic
```

## Running

You can then run it with

```shell
TimeBasic 09:00 < <CSV file>
```

The first argument (09:00 in the above) is the time when you want the schedule to start, in 24-hour format. Here, I am starting at 9:00 a.m.

You can see a sample CSV file in the csv_files directory.

This produces a LaTeX file dailySchedule.tex. Run

```shell
pdflatex daily-planner.tex
```

to produce a PDF schedule. (The file daily-planner.tex includes dailySchedule.tex.)