{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
import System.IO
import System.Environment
import Data.Text as T (unpack, pack, concat)
import Data.Time.LocalTime
import Data.Time.Format
import Data.Time.Clock
import Debug.Trace
import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString.Lazy.Char8 as C8
import Control.Monad
import qualified Data.Vector as V

import Text.LaTeX.Base.Commands
import Text.LaTeX.Base
import Text.LaTeX.Base.Syntax as S

import Data.List (find)
import Data.Csv
import Data.Maybe (catMaybes)

-- Constants

slotPeriod :: DiffTime
slotPeriod = secondsToDiffTime (60 * 15)

fillColor :: LaTeX
fillColor = TeXComm "cellcolor" [FixArg (TeXRaw "mylightgray")]

fmtHM :: TimeOfDay -> String
fmtHM = formatTime defaultTimeLocale "%l:%M"

endTime :: TimeOfDay
endTime = TimeOfDay 21 45 0

timezone :: TimeZone
timezone = hoursToTimeZone (-5)



makeLine :: Bool -> Bool -> LaTeX
makeLine hour inEvent = 
    let name = if hour then "blackline" else "grayline" in
      let linecolor = if hour then "black" else "anti-flashwhite" in
        let end = if inEvent then "mylightgray" else linecolor in
          TeXComm name [FixArg (TeXRaw (pack end))] <> "\n"

decrement :: TimeOfDay -> TimeOfDay
decrement (TimeOfDay h m s) = TimeOfDay nh nm s
                                where zm = m - 15
                                      (nh, nm) = if zm < 0
                                                 then (h - 1, zm + 60)
                                                 else (h, zm)

addFifteen:: TimeOfDay -> TimeOfDay
addFifteen s = timeToTimeOfDay $ (timeOfDayToTime s) + slotPeriod

overlap :: Event -> TimeOfDay -> Int
overlap Event {start=eStart, end=eEnd} pStart = 
  let pe = (timeOfDayToTime pStart) + slotPeriod in
  let [es, ee, ps] = Prelude.map timeOfDayToTime [eStart, eEnd, pStart] in
  quot (round ((min pe ee) - (max es ps))) 60

makeGrid :: [Event] -> TimeOfDay -> TimeOfDay -> LaTeX
makeGrid events dawn last = 
  let inEv = any (\e -> last > (start e) && last < (end e)) events in
  let label = if (todMin last) `mod` 30 == 0
              then TeXRaw $ pack $ fmtHM last
              else mempty
  in
  let color = if any (\e -> overlap e last > 0) events
              then fillColor else mempty
  in
  let name = case find (\e -> start e >= last && start e < addFifteen last)
                  events
                  of 
                    Just e -> TeXRaw $ T.concat ((Main.title e):(map pack
                        [":\t", fmtHM $ start e, " - ", fmtHM (end e)]))
                    Nothing -> mempty
  in
  -- I changed from name & color to (name <> color) in the next line because of
  -- a bug and to imitate the last line
  if dawn == last then (label & (name <> color)) & lnbk 
  else (makeGrid events dawn (decrement last)) <>
       (makeLine (todMin last `mod` 60 == 0) inEv) <>
       label & (name <> color) & lnbk


-- twenty = TimeOfDay 10 20 0
-- fifty = TimeOfDay 10 50 0

data Event =
  Event { title :: Text,
          start :: TimeOfDay,
          end :: TimeOfDay
        } deriving (Show)

instance FromRecord (Maybe Event) where
  parseRecord v
    | length v >= 3 = Just <$> (Event <$>
                        (T.pack . C8.unpack) <$> (v .! 0) <*>
                        (parseTimeOfDay <$> (v .! 1)) <*>
                        (parseTimeOfDay <$> (v .! 2)))
    | otherwise     = return Nothing


parseTimeOfDay :: String -> TimeOfDay
parseTimeOfDay = parseTimeOrError True defaultTimeLocale "%H:%M"

main = do
    contents <- BS.getContents
    args <- getArgs
    let startTimeString = head args
    let startTime = parseTimeOrError True defaultTimeLocale "%H:%M" startTimeString
    case decode NoHeader contents of
      Left msg     -> putStrLn msg
      Right events ->
        let eee = (catMaybes . V.toList) events in
        let tex = makeGrid eee startTime endTime in
        renderFile "dailySchedule.tex" tex
        
{- To read the date as well, with a def (so this would require a change in daily-planner.tex

parseTimeOfDay s = snd $ utcToLocalTime (hoursToTimeZone (-6)) (parseTimeOrError True defaultTimeLocale "%Y-%m-%dT%H:%M:%SZ" s)

-- toGregorian is in Data.Time.Calendar
getDayNumbers eee = reverse $ toGregorian $ localDay $ head $ eee


makeDefs eee startTime endTime = 
  TeXComm "newDate" (FixArg current):(map (FixArg . show) ((getDay eee)))
  <>
  TeXComm "newcommand" [FixArg (TeXComm "scheduleDate"), FixArg (TexComm "displaydate" [FixArg (TeXRaw "current")])]
  <> 
  TeXComm "def" [TeXComm "schedule" [],
                 FixArg (makeGrid (map localTimeOfDay eee) startTime endTime)]


            Just eee -> makeDefs eee startTime endTime

-}
